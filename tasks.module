<?php

/**
 * @file
 * Tasks module.
 * 
 * @TODO: Drush integration.
 * @TODO: Views integration.
 * @TODO: Rules integration.
 */

/**
 * Implements hook_menu().
 */
function tasks_menu() {
  $items['tasks/%user_uid_optional'] = array(
    'title' => 'Tasks',
    'access callback' => 'tasks_access',
    'access arguments' => array(1),
    'file' => 'tasks.pages.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tasks_management_form', 1),
  );

  return $items;
}

/**
 * Access callback to check access permission to /tasks/%uid page.
 * 
 * @param object $account User object
 */
function tasks_access($account) {
  if (!user_is_anonymous()) {
    return $account->uid == $GLOBALS['user']->uid;
  }
  return FALSE;
}

/**
 * Get user's task-list.
 *
 * @param int $uid
 * @param boolean $refresh 
 */
function tasks_get_lists($uid, $refresh = FALSE) {
  if (!$refresh && ($cache = cache_get("tasks_lists_{$uid}"))) {
    return $cache->data;
  }

  $select = db_select('tasks_list', 'tl');
  $select = $select->fields('tl', array('lid', 'title'));
  $select = $select->condition('uid', $uid);
  $select = $select->orderBy('title');
  $select = $select->execute();
  $lists = array();
  foreach ($select as $list) {
    $lists[$list->lid] = $list->title;
  }

  cache_set("tasks_lists_{$uid}", $lists);
  
  return $lists;
}

/**
 * Get task-list items. Have to specify list ID or user ID.
 *
 * @param int $lid Load tasks by list.
 * @param int $uid Load tasks by user ID.
 * @param boolean $refresh
 * @return array of tasks.
 */
function tasks_get_items($lid = NULL, $uid = NULL, $refresh = FALSE) {
  if (!$refresh && ($cache = cache_get("tasks_list_{$lid}"))) {
    return $cache->data;
  }
  
  $select = db_select('tasks_item', 'ti');
  $select = $select->fields('ti', array('iid', 'lid', 'priority', 'completed', 'uid', 'due', 'link', 'title', 'body'));
  $select = $select->condition('ti.lid', $lid);
  $select = $select->orderBy('ti.priority');
  $select = $select->execute();
  $items = array();
  foreach ($select as $item) {
    $items[$item->iid] = $item;
  }
  return $items;
}

/**
 * Implements hook_theme().
 */
function tasks_theme() {
  $themes['tasks_management_form'] = array(
    'render element' => 'form',
    'file' => 'tasks.pages.inc',
  );
  return $themes;
}

/**
 * Save task-list.
 * 
 * @param type $list
 */
function tasks_list_save($list) {
  if (isset($list->lid)) {
    $return = drupal_write_record('tasks_list', $list, 'lid');
  }
  else {
    $return = drupal_write_record('tasks_list', $list);
  }
  return $return ? $list : $return;
}

/**
 * Load task-list.
 * 
 * @param int $lid
 * @param string $type uncompleted/completed/all
 */
function tasks_list_load($lid, $type = TRUE) {
  $select = db_select('tasks_list', 'tl');
  $select->fields('ti', array('lid', 'uid', 'title'));
  $select->condition('ti.iid', $iid);
  $select = $select->execute();
  return $select->fetchObject();
}

/**
 * Delete task-list.
 * 
 * @param int $lid
 */
function tasks_list_delete($lid) {
  # Set LID of tasks to null.
  $update = db_update('tasks_item');
  $update->fields(array('lid' => NULL));
  $update->condition('lid', $lid);
  $update->execute();
  
  # Remove list in database.
  $delete = db_delete('tasks_list');
  $delete->condition('lid', $lid);
  $delete->execute();
}

/**
 * Save task.
 * 
 * @param type $item 
 */
function tasks_item_save($item) {
  if (isset($item->iid)) {
    $return = drupal_write_record('tasks_item', $item, 'iid');
  }
  else {
    $return = drupal_write_record('tasks_item', $item);
  }
  return $return ? $item : $return;
}

/**
 * Load a task.
 * 
 * @param type $iid 
 */
function tasks_item_load($iid) {
  $select = db_select('tasks_item', 'ti');
  $select->fields('ti', array('iid', 'completed', 'lid', 'uid', 'due', 'priority', 'link', 'title', 'body'));
  $select->condition('ti.iid', $iid);
  $select = $select->execute();
  return $select->fetchObject();
}

/**
 * Make a task as completed.
 * 
 * @param type $iid 
 */
function tasks_item_check($iid) {
  if ($task = tasks_item_load($iid)) {
    $task->completed = 1;
    return tasks_item_save($task);
  }
  return FALSE;
}

/**
 * Make a task as uncompleted.
 *
 * @param type $iid 
 */
function tasks_item_uncheck($iid) {
  if ($task = tasks_item_load($iid)) {
    $task->completed = 0;
    return tasks_item_save($task);
  }
  return FALSE;
}

/**
 * Delete a task.
 *
 * @param int $iid
 */
function tasks_item_delete($iid) {
  db_delete('tasks_item')
    ->condition('iid', $iid)
    ->execute();
}
