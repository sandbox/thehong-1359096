<?php
/**
 * @file
 * 
 * Functions for tasks.module pages.
 */

function tasks_management_form($form, $form_state, $account, $list_id = NULL) {
  $form['#attached']['js'][] = drupal_get_path('module', 'tasks') . '/tasks.js';
  $form['#attached']['css'][] = drupal_get_path('module', 'tasks') . '/tasks.css';

  $form['tasks-lists'] = array(
    '#type' => 'select',
    '#title' => t('Lists'),
    '#options' => array('' => t('Default')) + tasks_get_lists($account->uid, TRUE),
    '#default_value' => (int)$list_id,
  );

  $form['tasks-list-new'] = array(
    '#type' => 'textfield',
    '#title' => t('New list'),
  );

  $form['tasks-items']['#tree'] = TRUE;
  foreach (tasks_get_items($list_id) as $item) {
    $form['tasks-items'][$item->iid]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 30,
      '#default_value' => $item->priority,
      '#attributes' => array('class' => array('tasks-item-priority'))
    );
    $form['tasks-items'][$item->iid]['completed'] = array(
      '#type' => 'checkbox',
      '#default_value' => $item->completed,
    );
    $form['tasks-items'][$item->iid]['title'] = array(
      '#type' => 'textfield',
      '#default_value' => $item->title,
    );
    $form['tasks-items'][$item->iid]['due'] = array(
      '#type' => 'textfield',
      '#title' => t('Due date'),
      '#default_value' => $item->due,
    );
    $form['tasks-items'][$item->iid]['body'] = array(
      '#type' => 'textarea',
      '#default_value' => $item->body,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


function theme_tasks_management_form($variables) {
  $form = &$variables['form'];
  
  $output = '';
  $output .= '<div id="tasks-management-left">';
  $output .= drupal_render($form['tasks-lists']);
  $output .= drupal_render($form['tasks-list-new']);
  $output .= '</div>';

  $rows = array();
  foreach (element_children($form['tasks-items']) as $k) {
    $rows[] = array(
      'data' => array(
        drupal_render($form['tasks-items'][$k]['completed']),
        drupal_render($form['tasks-items'][$k]['weight']),
        drupal_render($form['tasks-items'][$k]['title']),
        drupal_render($form['tasks-items'][$k]['due'])
          . drupal_render($form['tasks-items'][$k]['body']),
      ),
      'class' => array('draggable'),
    );
  }

  $right = theme('table', array(
    'header' => array(t('Completed'), t('Priority'), t('Title'), t('Detail')),
    'rows' => $rows,
    'attributes' => array('id' => 'tasks-manager-items-table'),
  ));
  
  drupal_add_tabledrag('tasks-manager-items-table', 'order', 'sibling', 'tasks-item-priority');

  $output .= '<div id="tasks-management-right">';
  $output .= $right;
  $output .= drupal_render($form['submit']);
  $output .= '</div>';
  $output .= drupal_render_children($form);

  return $output;
}
