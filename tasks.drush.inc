<?php

/**
 * @file
 * Drush integration
 */

/**
 * Implements hook_drush_help().
 */
function tasks_drush_help($section) {
  switch ($section) {
    case 'drush:tasks-list-create':
    case 'drush:tasks-list-delete':
    case 'drush:tasks-item-create':
      break;
  }
}

/**
 * Implements hook_drush_command().
 */
function tasks_drush_command() {
  $items['tasks-list-create'] = array(
    'description' => 'Create new task list',
    'arguments' => array(
      'name' => 'Name of list',
    ),
    'options' => array(
      'uid' => 'User ID. Default value is 1.',
    ),
    'callback' => 'tasks_drush_create_list',
    'aliases' => array('tlc'),
  );
  
  $items['tasks-list-delete'] = array(
    'description' => 'Delete task list',
    'arguments' => array(
      'lid' => 'List ID',
    ),
    'callback' => 'tasks_list_delete',
  );

  $items['tasks-item-create'] = array(
    'description' => 'Create a task',
    'arguments' => array(
      'title' => 'Task title',
    ),
    'aliases' => array('tic'),
    'callback' => 'tasks_drush_item_create',
    'options' => array(
      'title' => 'Task title. Required argument.',
      'uid' => 'User ID. Default is 1',
      'lid' => 'List ID. Default is NULL',
      'priority' => 'Task priority. Default is 0',
      'due' => 'Due date. Default is NULL',
      'title' => 'Task title',
      'body' => 'Task details',
      'link' => 'Related URL. Default is NULL',
    ),
  );
  
  $items['tasks-item-delete'] = array(
    'description' => 'Delete a task',
    'aliases' => array('tid'),
    'arguments' => array(
      'iid' => 'Task ID',
    ),
    'options' => array(
      'iid' => 'Task ID',
    ),
    'callback' => 'tasks_drush_item_delete',
  );

  return $items;
}

/**
 * Drush callback to create new task-list.
 * 
 * @param name $name
 * @return object task-list.
 */
function tasks_drush_create_list($name) {
  $list = new stdClass();
  $list->uid = drush_get_option('title');
  $list->uid = !empty($list->uid) ? $list->uid : 1;
  $list->title = check_plain($name);
  return tasks_list_save($list);
}

function tasks_drush_item_create($title = '') {
  $title = !empty($title) ? $title : drush_get_option('title');
  $title = trim($title);
  $title = check_plain($title);
  if (!empty($title)) {
    $uid = drush_get_option('uid');
    $uid = !empty($uid) ? $uid : 1;
    $lid = drush_get_option('lid');
    $lid = !empty($lid) ? $lid : NULL;
    $priority = drush_get_option('priority');
    $priority = !empty($priority) ? $priority : 0;
    $due = drush_get_option('due');
    if (!empty($due)) {
      $due = strtotime($due);
    }
    $body = drush_get_option('body');
    $body = trim($body);
    $body = check_plain($body);

    $task = new stdClass();
    $task->uid = $uid;
    $task->lid = $lid;
    $task->priority = $priority;
    $task->due = $due;
    $task->title = $title;
    $task->body = $body;
    
    tasks_item_save($task);
  }
  else {
    drush_set_error('Task title can not be empty.');
  }
}

function tasks_drush_item_delete($iid = NULL) {
  $iid = !empty($iid) ? $iid : drush_get_option('iid');
  if ($iid) {
    tasks_item_delete($iid);
  }
  else {
    drush_set_error('Task ID can not be empty.');
  }
}
